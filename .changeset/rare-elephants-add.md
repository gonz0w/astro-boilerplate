---
'@astrojs/telemetry': patch
---

Fix an issue where handled error output was piped to the user
